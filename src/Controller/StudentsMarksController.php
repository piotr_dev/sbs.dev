<?php

namespace App\Controller;

use App\Entity\Student;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StudentsMarksController extends AbstractController
{
    /**
     * @Route("/", name="students_marks")
     */
    public function index(): Response
    {
        return $this->render('students_marks/index.html.twig');
    }

    /**
     * Grazina masyva json pavidalu studentu pazymu vidurkiui atvaizduoti
     *
     * @Route("/students-marks-average-json", name="students_marks_average_json")
     *
     * @return JsonResponse
     */
    public function usersMarks(): JsonResponse{

        $studentsSubjectsMarksAverage = $this->getDoctrine()
            ->getRepository(Student::class)
            ->getStudentsSubjectsMarksAverage();

        $arr = [];

        foreach ($studentsSubjectsMarksAverage as $ssma){
            $arr[$ssma['id']]['fullname'] = $ssma['fullname'];
            $arr[$ssma['id']]['university_name'] = $ssma['university_name'];
            $arr[$ssma['id']][$ssma['subject_name']] = $ssma['average_mark'] ?? '';
        }

        $arr = array_values($arr);

        return $this->json($arr);
    }
}
