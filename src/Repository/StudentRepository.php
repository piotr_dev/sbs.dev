<?php

namespace App\Repository;

use App\Entity\Student;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use PhpParser\Node\Expr\Array_;

/**
 * @method Student|null find($id, $lockMode = null, $lockVersion = null)
 * @method Student|null findOneBy(array $criteria, array $orderBy = null)
 * @method Student[]    findAll()
 * @method Student[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Student::class);
    }

    /**
     * Grazina studentu masyva [[id, fullname, subject_name, average_mark, university_name], ...]
     * @return Array
     */
    public function getStudentsSubjectsMarksAverage(): Array
    {

        $s = $this->getEntityManager()
            ->getConnection()
            ->executeQuery("
                SELECT
                    s.id,
                    CONCAT(IFNULL(s.first_name, ''), ' ', IFNULL(s.last_name, '')) AS fullname,
                    s1.code as subject_name,
                    (SELECT ROUND(SUM(m.mark)/COUNT(m.mark), 1) FROM mark m WHERE m.student_id=s.id AND s1.id=m.subject_id) AS average_mark,
                    (SELECT u.name FROM university u WHERE u.id=s.university_id) as university_name
                   
                FROM student s
                CROSS JOIN subject s1  
                ORDER BY s.id, s1.id
            ")->fetchAll();

        return $s;
    }

}
