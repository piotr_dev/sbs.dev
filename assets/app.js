import "regenerator-runtime/runtime";
import ReactDOM from 'react-dom';
import React from 'react';
import StudentMarksTable from './js/StudentMarksTable.js';


ReactDOM.render(<StudentMarksTable />, document.getElementById('root'));
