import React, { useState, useEffect } from "react";
import axios from  "axios";

function StudentMarksTable() {

    const headerCells = [
        { name: "university_name", title: 'Universiteto pavadinimas' },
        { name: "fullname", title: 'Vardas Pavardė' },
        { name: "discrete_mathematics", title: 'Diskrečioji matematika' },
        { name: "object_oriented_programming", title: 'Objektinis programavimas' },
        { name: "philosophy", title: 'Filosofija' },
        { name: "english", title: 'Anglų k.' },
        { name: "project_management", title: 'Projektų valdymas' }
    ]

    const [data, setData] = useState([]);

    useEffect(()=>{
        (async () => {
            const result = await axios("/students-marks-average-json");
            setData(result.data);
        })();
    }, [])


    return (
        <table className='table table-bordered table-hover d-block mt-5 w-auto'>
            <thead>
                <tr>
                    {headerCells.map((headerCell) => (
                        <th key={headerCell.name} scope='col'>{headerCell.title}</th>
                    ))}
                </tr>
            </thead>
            <tbody>
                {data.map((row, i) => (
                    <tr key={i}>
                        {headerCells.map((headerCell, i) => (
                            <td key={headerCell.name}>{row[headerCell.name]}</td>
                        ))}
                    </tr>
                ))}
            </tbody>
        </table>
    );
}


export default StudentMarksTable;

